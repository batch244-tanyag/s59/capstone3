import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

    // Allows us to consume the User context object and its properties to use for user validation
    const { user, setUser } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

	function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();



        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data.access !== undefined) {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Car4U!"
                });
            } else {
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again!"
                })
            }
        })



        setEmail('');
        setPassword('');

    

    }

   

    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Changes the global user state tos tore the "id" and the "idAdmin" property of the user which will be used for validation across the whole application
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


	useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    return (


        (user.id !== null)
        ?
            <Navigate to='/products'/>
        :
    	// Invokes the authenticate function upon clicking on submit button

        <Form onSubmit={(e) => authenticate(e)}>
         <h3>Log-in Here</h3>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    required
                    value={email}
	                onChange={(e) => setEmail(e.target.value)}
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    required
                    value={password}
	                onChange={(e) => setPassword(e.target.value)}
                />
            </Form.Group>
            <p></p>

            { isActive 
            	? 
	                <Button variant="success" type="submit" id="submitBtn">
	                    Submit
	                </Button>
                : 
	                <Button variant="success" type="submit" id="submitBtn" disabled>
	                    Submit
	                </Button>
			}
        </Form>
    )
}